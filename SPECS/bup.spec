Name:           bup
Version:        0.28.1
Release:        0%{?dist}
Summary:        A Backup Tool based on git
Group:          Applications/Other

License:        LGPLv2
URL:            https://bup.github.io/
Source0:        https://github.com/bup/bup/archive/0.28.1.tar.gz
Patch:          bup.allow-pylibs-in-share.patch

BuildRequires: python2-devel
BuildRequires: git
Requires:      fuse-python
Requires:      pyxattr
Requires:      pylibacl
Requires:      git
BuildRequires: perl-Time-HiRes
BuildRequires: pandoc


%description
Very efficient backup system based on the git packfile format, providing
fast incremental saves and global deduplication (among and within files,
including virtual machine images).

%prep
%setup
%patch

%build
./configure
%{__make} %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
%{__make} install \
    MANDIR=%{buildroot}%{_mandir} \
    DOCDIR=%{buildroot}%{_docdir}/%{name} \
    BINDIR=%{buildroot}%{_bindir} \
    LIBDIR=%{buildroot}%{_datadir}/%{name}


%clean
rm -rf %{buildroot}


%files
%doc README LICENSE
%{_mandir}/man1/*
%{_docdir}/bup/*
%{_datadir}/bup/cmd/*
%{_datadir}/bup/bup/*
%{_datadir}/bup/web/*
%{_bindir}/bup


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Sat Nov 26 2016 Update to bup 0.28.1
* Tue Dec 15 2015 Add patch to allow installation to /usr/share 0.27-2
* Mon Dec 14 2015 Fixes for EPEL builds 0.27-1
* Mon Dec 14 2015 Initial spec file for version 0.27-0
- 
