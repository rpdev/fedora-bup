Name:           bup-kup
Version:        0.6.1
Release:        0%{?dist}
Summary:        Kup is a KDE-based frontend for the bup backup software
Group:          Applications/Other

License:        GPLv2
URL:           	https://github.com/spersson/Kup
Source0:        https://github.com/spersson/Kup/archive/kup-0.6.1.tar.gz

BuildRequires: cmake
BuildRequires: extra-cmake-modules
Requires:      qt5-qtbase
BuildRequires: qt5-qtbase-devel
Requires:      kf5-kcoreaddons
BuildRequires: kf5-kcoreaddons-devel
Requires:      kf5-kdbusaddons
BuildRequires: kf5-kdbusaddons-devel
Requires:      kf5-ki18n
BuildRequires: kf5-ki18n-devel
Requires:      kf5-kio
BuildRequires: kf5-kio-devel
Requires:      kf5-solid
BuildRequires: kf5-solid-devel
Requires:      kf5-kidletime
BuildRequires: kf5-kidletime-devel
Requires:      kf5-knotifications
BuildRequires: kf5-knotifications-devel
Requires:      kf5-kiconthemes
BuildRequires: kf5-kiconthemes-devel
Requires:      kf5-kconfig
BuildRequires: kf5-kconfig-devel
Requires:      kf5-kinit
BuildRequires: kf5-kinit-devel
Requires:      openssl
BuildRequires: openssl-devel
Requires:      bup
Requires:      rsync


%description
Kup is created for helping people to keep up-to-date backups of their
personal files. Connecting a USB hard drive is the primary supported
way to store files, but saving files to a server over a network
connection is also possible for advanced users.

When you plug in your external hard drive Kup will automatically start
copying your latest changes, but of course it will only do so if you
have been active on your computer for some hourse since the last
time you took a backup (and it can of course ask you first,
before copying anything). In general Kup tries to not disturb
you needlessly.

There are two types of backup schemes supported, one which
keeps the backup folder completely in sync with what you
have on your computer, deleting from the backup any file
that you have deleted on your computer etc. The other scheme
also keeps older versions of your files in the backup folder.
When using this, only the small parts of your files that has
actually changed since last backup will be saved and therefore
incremental backups are very cheap. This is especially useful
if you are working on big files. At the same time it's as easy
to access your files as if a complete backup was taken every
time; every backup contains a complete version of your
directories. Behind the scenes all the content that is actually
the same is only stored once. To make this happen Kup runs the
backup program "bup" in the background, look at
https://github.com/bup/bup for more details.


%prep
%setup -n Kup-kup-%{version}

%build
%cmake -DPLUGIN_INSTALL_DIR=%{_libdir}/qt5/plugins .
%{__make} %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
%{__make} install DESTDIR=%{buildroot}

%check
ctest -V %{?_smp_mflags}

%clean
rm -rf %{buildroot}

%files
%doc README.md LICENSE
%{_bindir}/*
%{_sysconfdir}/xdg/autostart/*
%{_libdir}/lib*.so*
%{_libdir}/qt5/plugins/*.so
%{_datadir}/icons/hicolor/*/*/*.*
%{_datadir}/knotifications5/kupdaemon.notifyrc
%{_datadir}/kservices5/bup.protocol
%{_datadir}/kservices5/kcm_kup.desktop
%{_datadir}/locale/*/LC_MESSAGES/kup.mo


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Sat Nov 26 2016 Update to kup 0.6.1-0
* Tue Dec 15 2015 Fix installation location of plugins 0.6.0-1
* Tue Dec 15 2015 Initial spec for version 0.6.0-0
-
